# Simple Mqtt Client
## Description
This is simple mqtt client software.

## Environment
- C# (.Net 4.5.2)
- MQTTnet (from NuGet)

## ScreenShot
![ss1](https://gitlab.com/secondpc.sc/simplemqttclient/raw/images/ss1.PNG)


## TODO
- SSL対応
- データの自動整形（CSVなど）
- データ送信履歴機能
- サブスクライブメッセージの色分け
- RestAPIへの対応（beebotte対応）
- Retain機能追加（取り消しの方法を調べる）
- reconnect時のサブスクライブクリア
- 未コネクトでのパブサブボタン無効化