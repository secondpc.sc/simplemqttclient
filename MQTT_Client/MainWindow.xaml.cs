﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MQTTnet;
using MQTTnet.Client;
using System.Text.RegularExpressions;
using System.Windows.Controls.Primitives;
using MQTTnet.Protocol;
using System.Windows.Threading;
using System.IO;

namespace MQTT_Client {
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window {
        IMqttClient mqttClient = new MqttFactory().CreateMqttClient();
        List<Topic> topics = new List<Topic>();
        List<ReceivedMessage> rmsg = new List<ReceivedMessage>();
        private Dispatcher _dispatcher = null;
        const int LIMIT = 1000;
        string setting_path = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\SimpleMqttClient";
        string setting_fname = "tmp.csv";

        class AutoCompleteSource{
            public string HOST { get; set; }
            public string PORT { get; set; }
            public string USERNAME { get; set; }

            public AutoCompleteSource(string host, string port, string username) {
                HOST = host;
                PORT = port;
                USERNAME = username;
            }
        };
        const int BUFFER_LENGTH = 5;
        int buff_index = 0;
        bool save_flag = false;
        List<AutoCompleteSource> auto_complete_source = new List<AutoCompleteSource>();

        public MainWindow() {
            InitializeComponent();
            _dispatcher = Dispatcher.CurrentDispatcher;

            mqttClient.ApplicationMessageReceived += onMessage;
            // データグリッドにデータソースをバインド
            subscribe_topic_list.ItemsSource = topics;
            dg_received_list.ItemsSource = rmsg;
            //dg_received_list.Background = new SolidColorBrush(Colors.Aqua);

            t_port.Text = "1883";
            loadInputDataHistory(setting_path, setting_fname);
        }

        ~MainWindow() {
            DisConnectBroker();

            // save data
            if (save_flag) {
                saveInputDataHistory(setting_path, setting_fname);
            }   
        }

        private void onMessage (object sender, MqttApplicationMessageReceivedEventArgs e) {
            var topic = e.ApplicationMessage.Topic;
            var qos = (int)e.ApplicationMessage.QualityOfServiceLevel;
            //var client_id = e.ClientId;
            var message = e.ApplicationMessage.ConvertPayloadToString();

            _dispatcher.BeginInvoke(new Action(() =>
            {
                rmsg.Insert(0, new ReceivedMessage(topic, qos, message));
                if (rmsg.Count > LIMIT) {
                    rmsg.RemoveAt(rmsg.Count - 1);
                }
                dg_received_list.ItemsSource = null;
                dg_received_list.ItemsSource = rmsg;
                //t_received_msg.Text = msg + t_received_msg.Text;
            }));
                
            //Console.WriteLine("{0}, {1}", topic, message);
        }

        private async void DisConnectBroker() {
            // 不要かも
            if (mqttClient.IsConnected) {
                await mqttClient.DisconnectAsync();
            }
        }

        private MQTTnet.Serializer.MqttProtocolVersion fetchMqttProtocolVersion() {
            var i = c_mqtt_ver.SelectedIndex;
            if (i == 0) {
                return MQTTnet.Serializer.MqttProtocolVersion.V311;
            }
            else if (i == 1) {
                return MQTTnet.Serializer.MqttProtocolVersion.V310;
            }
            else {
                // 不明な場合はV311を返す
                return MQTTnet.Serializer.MqttProtocolVersion.V311;
            }
        }

        private void saveInputDataHistory(string dir, string fname) {
            if (!Directory.Exists(dir)) {
                Directory.CreateDirectory(dir);
            }
            var path = System.IO.Path.Combine(dir, fname);
            //path, overwrite, encode
            StreamWriter writer = new StreamWriter(path, false, Encoding.UTF8);

            writer.Write("HOST, PORT, USERNAME\r\n");
            foreach (var e in auto_complete_source) {
                writer.Write(e.HOST + "," + e.PORT + "," + e.USERNAME + "\r\n");
            }
            writer.Close();
        }

        private void loadInputDataHistory(string dir, string fname) {
            if (!Directory.Exists(dir)) {
                return;
            }
            var path = System.IO.Path.Combine(dir, fname);

            StreamReader reader = new StreamReader(path, Encoding.UTF8);
            reader.ReadLine(); // ヘッダ読み飛ばし
            try {
                while (!reader.EndOfStream) {
                    var fields = reader.ReadLine().Split(',');
                    updateAutoCompleteBuffer(fields[0], fields[1], fields[2]);
                }
            }
            finally {
                reader.Close();
            }
        }

        private void updateAutoCompleteBuffer(string addr = null, string port = null, string uname = null) {
            if (addr == null) {
                addr = t_addr.Text;
            }
            if (port == null) {
                port = t_port.Text;
            }
            if (uname == null) {
                uname = t_username.Text;
            }

            if (auto_complete_source.Count != 0) {
                var search_index = buff_index - 1;
                if (search_index < 0) {
                    search_index = BUFFER_LENGTH - 1;
                }
                
                // 連続登録を避ける
                var flag = false;
                if (auto_complete_source[search_index].HOST != addr) {
                    flag = true;
                }
                if (auto_complete_source[search_index].PORT != port) {
                    flag = true;
                }
                if (auto_complete_source[search_index].USERNAME != uname) {
                    flag = true;
                }
                if (flag == false) {
                    //Console.WriteLine("Skip");
                    return;
                }
            }

            // 統合情報のアップデート（TODO: 改善の余地あり）
            if (auto_complete_source.Count < BUFFER_LENGTH) {
                auto_complete_source.Add(new AutoCompleteSource(addr, port, uname));
            }
            else {
                auto_complete_source[buff_index] = new AutoCompleteSource(addr, port, uname);
            }
            buff_index++;
            if (buff_index == BUFFER_LENGTH) {
                buff_index = 0;
            }
            /*
            foreach (var h in auto_complete_source) {
                Console.WriteLine("{0}, {1}, {2}", h.HOST, h.PORT, h.USERNAME);
            }
            Console.WriteLine("");
            */

            refleshAutoCompleteBuffer();

            if (mqttClient.IsConnected) {
                save_flag = true;
            }
        }
        
        private void refleshAutoCompleteBuffer() {
            // なんちゃってオートコンプリートのアップデート
            t_username.Items.DetachFromSourceCollection();
            var l_uname = from n in auto_complete_source select n.USERNAME;
            t_username.ItemsSource = l_uname.Distinct();

            t_port.Items.DetachFromSourceCollection();
            var l_port = from n in auto_complete_source select n.PORT;
            t_port.ItemsSource = l_port.Distinct();

            t_addr.Items.DetachFromSourceCollection();
            var l_host = from n in auto_complete_source select n.HOST;
            t_addr.ItemsSource = l_host.Distinct();
        }

        private async void connect2Broker(object sender, RoutedEventArgs e) {
            // 既に接続されている場合は確認する
            if (mqttClient.IsConnected) {
                var msg = "Already connected other broker server!．\nDo you want to disconnect and reconnect another broker server?";
                MessageBoxResult result = MessageBox.Show(msg, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No) {
                    return;
                }
                else {
                    // 不要かも
                    DisConnectBroker();
                }
            }
            btn_connect.IsEnabled = false;

            // パラメータ取得
            var mqtt_ver = fetchMqttProtocolVersion();
            var username = t_username.Text;
            var password = t_password.Password; //改善の余地あり
            if (password == "") {
                password = null;
            }

            // オプション作成            
            var options = new MqttClientOptionsBuilder()
                .WithClientId(t_client_id.Text)
                .WithTcpServer(t_addr.Text, int.Parse(t_port.Text))
                .WithProtocolVersion(mqtt_ver)
                .WithCredentials(username, password)
                //.WithKeepAlivePeriod(TimeSpan.FromSeconds(60))
                .Build();
            //cleansession調べる

            ((TextBlock)StatusBar.Items[0]).Text = "Connecting...";
            try {
                await mqttClient.ConnectAsync(options);
                ((TextBlock)StatusBar.Items[0]).Text = "Success to connect " + t_addr.Text + ":" + int.Parse(t_port.Text);
                var msg = "Success to Connect Broker!\n" + "\nHostName: " + t_addr.Text + "\nPort: " + int.Parse(t_port.Text);
                MessageBox.Show(msg, "Sccess to connect!", MessageBoxButton.OK, MessageBoxImage.Information);
                updateAutoCompleteBuffer();
            }
            catch (Exception error) {
                ((TextBlock)StatusBar.Items[0]).Text = "Failed to connect " + t_addr.Text + ":" + int.Parse(t_port.Text);
                // エラーメッセージをダイアログで表示
                MessageBox.Show(error.Message, "Failed to connect!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            btn_connect.IsEnabled = true;

        }


        private void t_port_PreviewTextInput(object sender, TextCompositionEventArgs e) {
            //portは数字のみ受け付ける
            e.Handled = !new Regex("[0-9]").IsMatch(e.Text);
        }

        private void btnClearHistory(object sender, RoutedEventArgs e) {
            rmsg.Clear();
            dg_received_list.ItemsSource = null;
            dg_received_list.ItemsSource = rmsg;
        }

        private void removeTopic(object sender, RoutedEventArgs e) {
            var index = subscribe_topic_list.SelectedIndex;
            if (index < 0) {
                //範囲外選択
                return;
            }

            var msg = "Do you want to remove \"" + topics[index].SubscribedTopic + "\" ?";
            var result = MessageBox.Show(msg, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.No) {
                return;
            }
            stopSubscribe(topics[index].SubscribedTopic);
            topics.RemoveAt(index);
            subscribe_topic_list.ItemsSource = null;
            subscribe_topic_list.ItemsSource = topics;
        }

        private void clearAllTopics(object sender, RoutedEventArgs e) {
            if (topics.Count == 0) {
                return;
            }

            var msg = "Do you want to clear all topics?";
            var result = MessageBox.Show(msg, "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.No) {
                return;
            }
            stopSubscribe(topics);
            topics.Clear();
            subscribe_topic_list.ItemsSource = null;
            subscribe_topic_list.ItemsSource = topics;
        }

        private void add_subscribe_topic_Click(object sender, RoutedEventArgs e) {
            addSubscribeTopic();
        }

        private void t_add_subscribe_topic_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter) {
                addSubscribeTopic();
            }
        }

        private void addSubscribeTopic() {
            if (mqttClient.IsConnected == false) {
                var msg = "You have to connect broker server first.";
                MessageBox.Show(msg, "No connection", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            var candidate_topic = t_add_subscribe_topic.Text.Replace(" ", "");
            if (candidate_topic == "") {
                var msg = "Invalid topic name.";
                MessageBox.Show(msg, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (topics.Find(n => n.SubscribedTopic == candidate_topic) != null) {
                var msg = "This topic is already subscribed! \"" + candidate_topic + "\"";
                MessageBox.Show(msg, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                t_add_subscribe_topic.Text = "";
                return;
            }

            
            topics.Add(new Topic(candidate_topic, int.Parse(selected_qos_sub.Text)));
            startSubscribe(topics.Last().SubscribedTopic, topics.Last().QoS);
            t_add_subscribe_topic.Text = "";
            subscribe_topic_list.ItemsSource = null;
            subscribe_topic_list.ItemsSource = topics;
        }

        private async void startSubscribe(string topic, int QoS) {
            //Console.WriteLine("{0}, {1}", topic, QoS);
            await mqttClient.SubscribeAsync(new List<TopicFilter> {
                new TopicFilter(topic, (MqttQualityOfServiceLevel)Enum.ToObject(typeof(MqttQualityOfServiceLevel), QoS))
            });
        }


        private async void stopSubscribe(string topic) {
            await mqttClient.UnsubscribeAsync(topic);
        }

        private void stopSubscribe(List<Topic> topics) {
            foreach (var t in topics) {
                stopSubscribe(t.SubscribedTopic);
            }
        }

        private void btn_publish_Click(object sender, RoutedEventArgs e) {
            publishMesage();

        }

        private void t_publish_payload_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter) {
                publishMesage();
            }
        }

        private async void publishMesage() {
            if (mqttClient.IsConnected == false) {
                var msg = "You have to connect broker server first.";
                MessageBox.Show(msg, "No connection", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (t_publish_topic.Text.Replace(" ", "") == "") {
                MessageBox.Show("Invalid topic name.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var topic = t_publish_topic.Text;
            var QoS = int.Parse(selected_qos_pub.Text);
            var payload = t_publish_payload.Text;
            var message = new MqttApplicationMessageBuilder()
                .WithTopic(topic)
                .WithPayload(payload)
                .WithQualityOfServiceLevel((MqttQualityOfServiceLevel)Enum.ToObject(typeof(MqttQualityOfServiceLevel), QoS))
                .Build();

            await mqttClient.PublishAsync(message);
        }

        private void Button_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
            t_password_show.Text = t_password.Password;
            t_password_show.Visibility = Visibility.Visible;
            t_password.Visibility = Visibility.Hidden;
        }

        private void Button_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
            t_password_show.Text = "";
            t_password_show.Visibility = Visibility.Hidden;
            t_password.Visibility = Visibility.Visible;
        }

        private void btn_insert_json_template_Click(object sender, RoutedEventArgs e) {
            t_publish_payload.Text = "";
            t_publish_payload.Text = "{\"data\":\" your message here! \"}";
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e) {
            MenuItem menuitem = (MenuItem)sender; // オブジェクトをMenuItemクラスのインスタンスにキャストする。
            string header = menuitem.Header.ToString(); // Headerプロパティを取り出して、文字列に変換する。

            switch (header) {
                case "キャッシュの削除":
                    if (Directory.Exists(setting_path)) {
                        completeDelete(setting_path);                        
                    }
                    auto_complete_source.Clear();
                    refleshAutoCompleteBuffer();
                    MessageBox.Show("キャッシュの削除を完了しました", "キャッシュの削除", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;   
            }
        }


        //Assetsディレクトリ以下にあるTestディレクトリを削除
        /// <summary>
        /// 指定したディレクトリとその中身を全て削除する
        /// </summary>
        public static void completeDelete(string targetDirectoryPath) {
            if (!Directory.Exists(targetDirectoryPath)) {
                return;
            }

            //ディレクトリ以外の全ファイルを削除
            string[] filePaths = Directory.GetFiles(targetDirectoryPath);
            foreach (string filePath in filePaths) {
                File.SetAttributes(filePath, FileAttributes.Normal);
                File.Delete(filePath);
            }

            //ディレクトリの中のディレクトリも再帰的に削除
            string[] directoryPaths = Directory.GetDirectories(targetDirectoryPath);
            foreach (string directoryPath in directoryPaths) {
                completeDelete(directoryPath);
            }

            //中が空になったらディレクトリ自身も削除
            Directory.Delete(targetDirectoryPath, false);
        }
    }

    public class Topic{
        public string SubscribedTopic { get; set; }
        public int QoS { get; set; }

        public Topic(string topic, int QoS = 0) {
            this.SubscribedTopic = topic;
            this.QoS = QoS;
        }
    }

    public class ReceivedMessage {
        public string TOPIC { get; set; }
        public int QoS { get; set; }
        public string PAYLOAD { get; set; }
        public string showMessage { get; set; }

        public ReceivedMessage(string topic, int qos, string payload) {
            TOPIC = topic;
            QoS = qos;
            PAYLOAD = payload;
            showMessage = DateTime.Now.ToString() + Environment.NewLine;
            showMessage += "Topic: " + TOPIC + " / QoS: " + QoS + Environment.NewLine;
            showMessage += PAYLOAD;
        }
    }
}
